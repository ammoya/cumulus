from os import stat
from pwd import getpwuid, getpwnam
from grp import getgrgid
import stat as st_mask

class FileStats():
    def __init__(self, filename):
        try:
            self.stats = self.get_stats(filename)
        except OSError:
            raise OSError('File does not exist')
    def get_stats(self, filename):
        try:
            stats = stat(filename)
        except OSError:
            raise OSError('File doesn\'t exist')
        else:
            return stats

    def get_owner(self):
        return getpwuid(self.stats.st_uid).pw_name

    def get_group(self):
        return getgrgid(self.stats.st_gid).gr_name

    def user_writable(self):
        return bool(self.stats.st_mode & st_mask.S_IWUSR)

    def group_writable(self):
        return bool(self.stats.st_mode & st_mask.S_IWGRP)

class FileUser():
    def __init__(self, username):
        try:
            self.pwd_info = getpwnam(username)
        except IOError:
            return None

    def get_group(self):
        return getgrgid(self.pwd_info.pw_gid).gr_name

def check_permission(username, filename):
    finfo = FileStats(filename)
    uinfo = FileUser(username)
    if finfo and uinfo:
        is_owner = finfo.get_owner() == username
        is_group = uinfo.get_group() == finfo.get_group()

        return (is_owner and finfo.user_writable()) or (is_group and finfo.group_writable())
