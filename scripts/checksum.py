#!/usr/bin/python

import sys
import lib_checksum
import event_bus
import utils


def do(str_file):
    utils.generate_xml('hash_collector.xml', [str_file, lib_checksum.get_file_checksum(str_file)])
    event_bus.create_node()
    event_bus.send_xml('hash_collector.xml')

do('testfile.txt')
