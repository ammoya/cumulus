import os
from Crypto.Hash import SHA256

def get_file_checksum(filename):
  h = SHA256.new()
  chunk_size = 1024
  try:
    with open(filename, 'rb') as f:
      while True:
        chunk = f.read(chunk_size)
        if len(chunk) == 0:
          break
        h.update(chunk)
    return h.hexdigest()
  except:
    return False
