#!/usr/bin/python

#the first time you will have to generate the hash entry for the filenames selected

import json
import lib_checksum
from datetime import datetime

class HashTable:

    def __init__(self):
        self.filename = 'hash_table.json'
        self.filehashes = self.get_file_hashes()

    def get_file_hashes(self):
        try:
            with open(self.filename, 'r') as f:
                data = json.load(f)
            return data
        except IOError:
            return {}

    def store(self):
        try:
            with open(self.filename, 'r+') as f:
                json.dump(self.filehashes, f)
        except IOError:
            return -1

    #if not exists, create new entry
    def update_hash(self, filename):
        print "Updating hash"
        checksum = lib_checksum.get_file_checksum(filename)
        if checksum:
            self.filehashes[filename] = checksum
            return self.store()


    #return array with name of files which doesn't have the same hash value as calculed
    def compare_hashes(self):
        result = []
        for file, stored_hash in self.filehashes:
            cal_checksum = lib_checksum.get_file_checksum(filename)
            if lib_checksum.get_file_checksum(filename) is not stored_hash:
                result.append(file)
        return result
