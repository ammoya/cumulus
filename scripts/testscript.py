import unittest
import os

import lib_checksum
from check_user_permission import FileStats, FileUser, check_permission

class TestCheckSumImplementation(unittest.TestCase):
    def test_output_exists(self):
        self.assertTrue(lib_checksum.get_file_checksum("testfile.txt") is not None)

    def test_output_dont_exists(self):
        self.assertFalse(lib_checksum.get_file_checksum("testfake"))

    def test_output_is_sha256(self):
        post = "343a8044abe4a8d903c7d429dc906c988caf46b7e34927c7846867245dd4a075"
        self.assertEquals(lib_checksum.get_file_checksum("testfile.txt"), post)
    def test_output_is_same_for_same_file(self):
        self.assertEquals(lib_checksum.get_file_checksum("testfile.txt"),lib_checksum.get_file_checksum("testfile.txt"))

    def test_output_is_different_file(self):
        self.assertNotEqual(lib_checksum.get_file_checksum("testfile.txt"),lib_checksum.get_file_checksum("testfile.1.txt"))

    def test_output_is_not_same_for_same_file_with_diff_name(self):
        self.assertNotEqual(lib_checksum.get_file_checksum("testfile.txt"),lib_checksum.get_file_checksum("testfile1.txt"))

class TestFileStats(unittest.TestCase):
    def setUp(self):
        self.testfile = FileStats("testfile.txt")

    def test_not_existent_file(self):
        with self.assertRaises(OSError):
            FileStats("falseFile.txt")

    def test_existent_file(self):
        self.assertTrue(FileStats("testfile.txt") is not None)

    def test_get_owner(self):
        self.assertTrue(type(self.testfile.get_owner()) is str)

    def test_get_group(self):
        self.assertTrue(type(self.testfile.get_group()) is str)


if __name__ == '__main__':
    unittest.main()
