from datetime import datetime
import time
from xml.etree import ElementTree as ET

def totimestamp(dt, epoch=datetime(1970,1,1)):
    td = dt - epoch
    # return td.total_seconds()
    return (td.microseconds + (td.seconds + td.days * 86400) * 10**6) / 10**6

def generate_xml(xml_file, args):
    ns = {'Event': "http://www.slaatsoi.org/eventschema"}
    ET.register_namespace('', "http://www.slaatsoi.org/eventschema")

    tree = ET.parse(xml_file)
    root = tree.getroot()
    #change eventID
    eventID = root.find('Event:ID', ns)

    eventContext = root.find('Event:EventContext', ns)
    #updatetime
    time = eventContext.find('Event:Time', ns)
    now = datetime.utcnow()
    time.find('Event:Timestamp', ns).text =  str(totimestamp(now))
    time.find('Event:CollectionTime', ns).text = now.isoformat()
    time.find('Event:ReportTime', ns).text = now.isoformat()

    #eventpayload
    eventPayload = root.find('Event:EventPayload', ns)
    interaction = eventPayload.find('Event:InteractionEvent', ns)
    #change operationName and operationID
    #change values
    parameters = interaction.find('Event:Parameters', ns)

    simple0 = parameters[0].find('Event:Simple', ns)
    for i in range(0, len(args)):
        parameters[i].find('Event:Simple', ns).find('Event:Value', ns).text = args[i]

    tree.write(xml_file)
