##COMMAND TO STORE THE SYSCALL
##sudo auditctl -w /home/alicia/testnotify/test_file.txt -p wa -k /home/alicia/testnotify/test_file.txt

##AUDIT.LOG LINE EXAMPLE
##type=SYSCALL msg=audit(1436427139.608:731): arch=c000003e syscall=2 success=yes exit=3 a0=d5a140 a1=441 a2=1b6 a3=7f8f6f47e7b8 items=2 ppid=5318 pid=5348 auid=4294967295 uid=1000 gid=1000 euid=1000 suid=1000 fsuid=1000 egid=1000 sgid=1000 fsgid=1000 tty=pts4 ses=4294967295


import re
import subprocess
import pwd
import monitor_checksum
import utils
import pubsub_client

def do_monitoring():
    f = subprocess.Popen(['tail', '-F', '/var/log/audit/audit.log'], stdout=subprocess.PIPE)
    #check error/stopevent to exit
    while True:
        line = f.stdout.readline()
        pattern = r'type=SYSCALL .*? syscall=2 success=(yes|no) .* uid=(\d*) .* key=\"(.*)\"'
        match_line = re.match(pattern, line)
        if match_line:
            #achieved
            print match_line.group(1)
            achieved = True if match_line.group(1) == 'yes' else False
            #user
            user = pwd.getpwuid(int(match_line.group(2))).pw_name
            #filename
            filename = match_line.group(3)


            utils.generate_xml('file_update.xml', [filename, user, str(achieved)])
            pubsub_client.use();
            if achieved:
                hash_table = monitor_checksum.HashTable()
                hash_table.update_hash(filename)


if __name__ == '__main__':
    do_monitoring()
